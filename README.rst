Description
===========

Daycare is a Django app that is keeping an eye on your site: 

- tracks 404 hits 
- lets you make redirects based on recorded 404 URLs 

It also can integrate with django-cms: 

- tracks URL changes for Pages 
- automatically adds (inactive) redirect when a Page changed it's url 

It also integrates nicely with django-admin.
