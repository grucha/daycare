from datetime import date, timedelta

from django.core.urlresolvers import reverse
from admin_tools.dashboard.modules import DashboardModule, AppList

from daycare.models import MissingURL, Redirect, URLHit


class DayCareStatusModule(DashboardModule):

    def __init__(self, **kwargs):
        super(DayCareStatusModule, self).__init__(**kwargs)
        self.title = kwargs.get('title', 'Daycare status')
        self.template = "daycare/dashboard_status_module.html"
        self.deletable = False
        self.apps = kwargs.get('apps', None)
        self.icons_dir = kwargs.get('icons_dir', '/static/admin_custom/icons/')

    def is_empty(self):
        return False

    def draft_redirects_count(self):
        return Redirect.objects.filter(status=Redirect.STATUS_DRAFT).count()

    def urls_without_redirects_count(self):
        urls = filter(
                      lambda url_obj: Redirect.objects.filter(catch_url=url_obj.url).count() > 0,
                      MissingURL.objects.all()
               )
        return len(urls)

    def urlhits_count_last_week(self):
        week_ago = date.today() - timedelta(days=7)
        return URLHit.objects.filter(date__gte=week_ago).count()

    def urlhits_count_last_month(self):
        month_ago = date.today() - timedelta(days=30)
        return URLHit.objects.filter(date__gte=month_ago).count()

    def init_with_context(self, context):
        # apps links
        for m in self.apps:
            # if type(m[0]) == ModelBase:
            if context.get("request").user.has_module_perms(m[0]._meta.app_label):
                m[0].admin_url = reverse('admin:%s_%s_changelist' %
                                         (m[0]._meta.app_label, m[0].__name__.lower()))

        return super(DayCareStatusModule, self).init_with_context(context)
