from daycare.middleware.tracking import Track404sMiddleware
from daycare.middleware.redirecting import RedirectFallbackMiddleware
