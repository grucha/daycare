# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'MissingURL'
        db.create_table('daycare_missingurl', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('hits', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('hits_last_week', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('hits_last_month', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal('daycare', ['MissingURL'])

        # Adding model 'URLHit'
        db.create_table('daycare_urlhit', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('url', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['daycare.MissingURL'])),
            ('date', self.gf('django.db.models.fields.DateField')(auto_now_add=True, blank=True)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
        ))
        db.send_create_signal('daycare', ['URLHit'])

        # Adding model 'Redirect'
        db.create_table('daycare_redirect', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('catch_url', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=255)),
            ('redirect_url', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('status', self.gf('django.db.models.fields.CharField')(default='Draft', max_length=20)),
            ('created_automatically', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
        ))
        db.send_create_signal('daycare', ['Redirect'])

        # Adding model 'PageURLTrack'
        db.create_table('daycare_pageurltrack', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('page_id', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('last_url', self.gf('django.db.models.fields.CharField')(default='', max_length=255)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime.now, blank=True)),
        ))
        db.send_create_signal('daycare', ['PageURLTrack'])


    def backwards(self, orm):
        # Deleting model 'MissingURL'
        db.delete_table('daycare_missingurl')

        # Deleting model 'URLHit'
        db.delete_table('daycare_urlhit')

        # Deleting model 'Redirect'
        db.delete_table('daycare_redirect')

        # Deleting model 'PageURLTrack'
        db.delete_table('daycare_pageurltrack')


    models = {
        'daycare.missingurl': {
            'Meta': {'ordering': "('-hits',)", 'object_name': 'MissingURL'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'hits': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hits_last_month': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hits_last_week': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'url': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'})
        },
        'daycare.pageurltrack': {
            'Meta': {'object_name': 'PageURLTrack'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'page_id': ('django.db.models.fields.IntegerField', [], {'default': '0'})
        },
        'daycare.redirect': {
            'Meta': {'object_name': 'Redirect'},
            'catch_url': ('django.db.models.fields.CharField', [], {'default': "''", 'unique': 'True', 'max_length': '255'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'created_automatically': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'redirect_url': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'default': "'Draft'", 'max_length': '20'})
        },
        'daycare.urlhit': {
            'Meta': {'object_name': 'URLHit'},
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'url': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['daycare.MissingURL']"})
        }
    }

    complete_apps = ['daycare']