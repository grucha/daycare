.. _installation:

Installation
============

* To install ::

    pip install 

* Add ``'daycare'`` to your ``INSTALLED_APPS`` setting::

    INSTALLED_APPS = (
        # other apps
        "daycare",
    )

* Add ``'daycare.middleware.RedirectFallbackMiddleware'`` at the top (or somewhere close to the top) of your ``MIDDLEWARE_CLASSES`` setting to handle redirects you've created
* Add ``'daycare.middleware.Track404sMiddleware'`` to track 404 hits
* Optionally, if you use django-cms, add ``'daycare.middleware.cmsmiddleware.CMSPageURLTrackMiddleware'`` to track Page url changes and create redirects automatically for them::

    MIDDLEWARE_CLASSES = (
        "daycare.middleware.RedirectFallbackMiddleware",

        # other middleware...

        "daycare.middleware.Track404sMiddleware",
        "daycare.middleware.cmsmiddleware.CMSPageURLTrackMiddleware",
    )

